#PROMPT='%{%(!.$fg_bold[red]%}⚡.$fg_bold[green]%}➜) %{$fg[cyan]%}%(4~.…/.)%3c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} %{$reset_color%}'
#RPROMPT='%(!.%{$fg[red]%}.%{$fg_bold[green]%}) %n@%m%{$reset_color%}'



ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%} "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%} ✘"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green] %}✔"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg[red]%} ➦"


ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[cyan]%} ✈"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%} ✭"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%} ✗"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%} ➦"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[magenta]%} ✂"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[grey]%} ✱"
ls


function get_env_color_by_hostname {
    if [[ $(hostname -s) = *-server ]]; then
        echo "%{$fg[red]%}"
    elif [[ $(hostname -s) = *-test ]]; then
        echo "%{$fg[green]%}"
    else
        echo "%{$reset_color%}"
    fi
}

function get_env_color_by_env_variable {

    if [[ -z "${ZSH_ENV_TYPE}" ]]; then
        get_env_color_by_hostname
    else
        if [[ ${ZSH_ENV_TYPE} = PROD ]]; then
            echo "%{$fg[red]%}"
        elif [[ ${ZSH_ENV_TYPE} = TEST ]]; then
            echo "%{$fg[green]%}"
        else
            echo "%{$reset_color%}"
        fi
    fi
}

#check if run on prod/test/dev/server and set color
function get_env_color {
    if [[ -z "${ZSH_ENV_TYPE}" ]]; then
        get_env_color_by_hostname
    else
        get_env_color_by_env_variable
    fi
}

function get_host {
#    Preferred editor for local and remote sessions
    if [[ -n $SSH_CONNECTION ]]; then
        echo "$(hostname):"
    fi
}


function prompt_char {
    if [ $UID -eq 0 ]
        then
            echo " %{$fg[red]%}✭%{$reset_color%} ";
        else
            case `uname` in
              Darwin)
                # commands for OS X go here
                echo "  ";
              ;;
              Linux)
                # commands for Linux go here
                echo "$(get_env_color) ᚛ %{$reset_color%}";
              ;;
              FreeBSD)
                # commands for FreeBSD go here
                echo "$(get_env_color) ᚛ %{$reset_color%}";
              ;;
            esac
    fi
}


# get the name of the branch we are on
function vim_bg_info() {
  if [[ "$(command echo $VIM)" != "" ]]; then
    echo " ⚡"
  fi
}



function draw_line_separator {
    echo "%(?.%{$fg[white]%}.%{$fg[red]%})%U${(l:COLUMNS:: :)?}%u%{$reset_color%}"
}


PROMPT='$(draw_line_separator)$(get_env_color)$(get_host)%{$reset_color%}%~%{$reset_color%}$(git_prompt_info)%{$reset_color%}$(git_prompt_ahead)%{$reset_color%}$(vim_bg_info)%(?,,%{$fg[red]%} FAIL %?%{$reset_color%})$(prompt_char)'



# Time on right
RPROMPT='%{$fg[black]%}%{$reset_color%}[%*]%{$reset_color%}'



# LS_COLORS documentation
# http://www.bigsoft.co.uk/blog/2008/04/11/configuring-ls_colors
#
LSCOLORS=Ehfxbxdxcxegedabagacad
case `uname` in
  Darwin)
  ;;
  Linux)
#    LS_COLORS='di=1;36;40:ln=35;40:so=31;40:pi=33;40:ex=32;40:bd=34;46:cd=34;43:su=0;41:sg=0;46:tw=0;42:ow=0;43:'
    LS_COLORS="${LS_COLORS}:di=01;36"
  ;;
  FreeBSD)
    LS_COLORS="${LS_COLORS}:di=01;36"
  ;;
esac