


# ...


ZSH_THEME="vld"

#
# set env type to get different colors for easy recognition
# PROD | TEST
#ZSH_ENV_TYPE="PROD"


source /etc/zsh_command_not_found

export GOPATH=$HOME/.go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin:$HOME/.local/bin:$HOME/.bin

if [ ! -z "$TERM" ]; then
    cat /var/run/motd        
fi
